//
// Created by ikmokhirio on 04.05.2019.
//

#include "iostream"
#include <sstream>
#include "Teacher.h"
#include "Person.h"
#include "Student.h"
#include <functional>
#include "Node.h"

using namespace std;

//Work with pointers only
#ifndef BINARY_TREE_BINARY_TREE_H
#define BINARY_TREE_BINARY_TREE_H

template<typename TYPE>
class Binary_tree {
private:
    Node<TYPE> *root;
    Node<TYPE> *current;

    int (*compare)(TYPE l, TYPE r);

    int size = 0;
public:
    explicit Binary_tree(TYPE *data, int (*function)(TYPE l, TYPE r) = [](TYPE l, TYPE r) {
        if (l > r) return 1;
        else if (l < r) return -1;
        else return 0;
    }) {
        compare = function;
        Node<TYPE> *new_node = new Node<TYPE>(data);
        root = new_node;
        size = 1;
    }

    Binary_tree(int (*function)(TYPE l, TYPE r) = [](TYPE l, TYPE r) {
        if (l > r) return 1;
        else if (l < r) return -1;
        else return 0;
    }) {
        compare = function;
        root = nullptr;
        size = 0;
    }

    Node<TYPE> *Get_root() {
        return root;
    }

    int Get_size() {
        return size;
    }

    bool Is_empty() {
        return (size <= 0);
    }

    void Add_element(TYPE *data) {
        if (!Is_empty()) {
            auto *new_node = new Node<TYPE>(data);
            current = root;

            while (true) {
                if (compare(*data, *(current->Get_data())) == 1) { //RIGHT >
                    if (current->Get_right() != nullptr) {
                        current = current->Get_right();
                    } else {
                        new_node->Set_parent(current);
                        current->Set_right(new_node);
                        break;
                    }
                } else { //LEFT  <=
                    if (current->Get_left() != nullptr) {
                        current = current->Get_left();
                    } else {
                        new_node->Set_parent(current);
                        current->Set_left(new_node);
                        break;
                    }
                }

            }
            size++;
        } else {
            Node<TYPE> *new_node = new Node<TYPE>(data);
            root = new_node;
            size = 1;
        }

    }

    bool Delete_element(TYPE *data) {
        current = Find_element(data, root);
        if (current == nullptr) {
            return false;
        }

        Node<TYPE> *parent_node;
        bool is_root = false;
        if (current != root) {
            parent_node = current->Get_parent(); //Parent node
        } else {
            parent_node = new Node<TYPE>(root->Get_data());
            is_root = true;
        }

        Node<TYPE> *this_node = current; //Node to delete

        if (current->Get_left() == nullptr) {
            if (is_root) {
                root = current->Get_right();
                this_node->Set_parent(nullptr);
                delete this_node;
                size--;
            } else if (current->Get_right() != nullptr) {
                if (compare(*(parent_node->Get_data()), *(current->Get_data())) != -1) { // <=
                    parent_node->Set_left(this_node->Get_right());
                } else {
                    parent_node->Set_right(this_node->Get_right());
                }
                (this_node->Get_right())->Set_parent(parent_node);
                this_node->Set_parent(nullptr);
                delete this_node;
                size--;
            } else {
                delete current;
                size--;
            }
            return true;
        }


        current = current->Get_left();

        while (current->Get_right() != nullptr) {
            current = current->Get_right();
        }

        if (current->Get_parent() == this_node) {
            if (compare(*(parent_node->Get_data()), *(current->Get_data())) != -1) {
                parent_node->Set_left(current);
            } else {
                parent_node->Set_right(current);
            }
            current->Set_right(this_node->Get_right());
            current->Set_parent(parent_node);
        } else {
            (current->Get_parent())->Set_right(current->Get_left());//Exclude from chain
            if (current->Get_left() != nullptr) {
                (current->Get_left())->Set_parent(current->Get_parent());
            }

            current->Set_right(this_node->Get_right());
            current->Set_left(this_node->Get_left());

            if (compare(*(parent_node->Get_data()), *(current->Get_data())) != -1) {
                parent_node->Set_left(current);
            } else {
                parent_node->Set_right(current);
            }
            current->Set_parent(parent_node);
        }
        if (is_root) {
            root = current;
            root->Set_parent(nullptr);
        }
        size--;
        return true;
    }

    void NLR(Node<TYPE> *node) {
        if (size <= 0) {
            throw 42;//Empty tree
        }

        current = node;

        if (node != nullptr) {
            if (node != root) {
                cout << "(\n" << *node->Get_data() << ") " << endl;
            } else {
                cout << "{\n" << *node->Get_data() << "} " << endl;
            }
        }
        if (node->Get_left() != nullptr) {
            NLR(node->Get_left());
        }
        if (node->Get_right() != nullptr) {
            NLR(node->Get_right());
        }
    }

    void LRN(Node<TYPE> *node) {

        if (size <= 0) {
            throw 42;//Empty tree
        }

        current = node;

        if (node->Get_left() != nullptr) {
            LRN(node->Get_left());
        }
        if (node->Get_right() != nullptr) {
            LRN(node->Get_right());
        }
        if (node != nullptr) {
            if (node != root) {
                cout << "(\n" << *node->Get_data() << ") " << endl;
            } else {
                cout << "{\n" << *node->Get_data() << "} " << endl;
            }
        }
    }

    void Map(Node<TYPE> *node, TYPE Function(TYPE)) { //Based on NLR
        if (Is_empty()) {
            throw 42;
        }
        current = node;
        if (node != nullptr) {
            node->Set_data(new TYPE(Function(*node->Get_data())));
        } else {
            return;
        }

        if (node->Get_left() != nullptr) {
            Map(node->Get_left(), Function);
        }
        if (node->Get_right() != nullptr) {
            Map(node->Get_right(), Function);
        }
    }

    //Where function
    Binary_tree<TYPE> Where(bool Function(TYPE)) {
        Binary_tree<TYPE> *new_tree = new Binary_tree<TYPE>;

        Where_add(new_tree, Function, root);

        return *new_tree;
    }

    void Where_add(Binary_tree<TYPE> *new_tree, bool Function(TYPE), Node<TYPE> *node) { //Add to arr
        current = node;
        if (node != nullptr) {
            if (Function(*(node->Get_data()))) {
                new_tree->Add_element(new TYPE(*node->Get_data()));
            }
        } else {
            return;
        }

        if (node->Get_left() != nullptr) {
            Where_add(new_tree, Function, node->Get_left());
        }
        if (node->Get_right() != nullptr) {
            Where_add(new_tree, Function, node->Get_right());
        }
    }
    //End

    Binary_tree<TYPE> Merge(Binary_tree<TYPE> merging_tree) {
        if (Is_empty() || merging_tree.Is_empty()) {
            throw 42;
        }
        Binary_tree<TYPE> new_tree;
        new_tree = *(this);
        Node<TYPE> *root_node = merging_tree.Get_root();
        current = new_tree.Get_root();
        while (true) {
            if (*(root_node->Get_data()) > *(current->Get_data())) {
                if (current->Get_right() != nullptr) {
                    current = current->Get_right();
                } else {
                    break;
                }
            } else {
                if (current->Get_left() != nullptr) {
                    current = current->Get_left();
                } else {
                    break;
                }
            }
        }

        if (compare(*(root_node->Get_data()), *(current->Get_data())) == 1) {
            current->Set_right(root_node);
            root_node->Set_parent(current);
        } else {
            current->Set_left(root_node);
            root_node->Set_parent(current);
        }
        size += merging_tree.Get_size();

        return new_tree;
    }

    //Getting subtree
    Binary_tree<TYPE> Get_subtree(Node<TYPE> *node) { //By node
        auto *new_tree = new Binary_tree<TYPE>;

        NLR_add(new_tree, node);

        return *new_tree;
    }

    Binary_tree<TYPE> Get_subtree(TYPE *data) { //By data
        auto *new_tree = new Binary_tree<TYPE>;


        NLR_add(new_tree, Find_element(data, root));

        return *new_tree;
    }

    void NLR_add(Binary_tree<TYPE> *new_tree, Node<TYPE> *node) { //Add to arr
        current = node;
        if (node != nullptr) {
            new_tree->Add_element(new TYPE(*node->Get_data()));
        } else {
            return;
        }

        if (node->Get_left() != nullptr) {
            NLR_add(new_tree, node->Get_left());
        }
        if (node->Get_right() != nullptr) {
            NLR_add(new_tree, node->Get_right());
        }
    }
    //End

    //Search for all elements with *Get_data() = data
    Node<TYPE> **Find_all_elements(TYPE *data, Node<TYPE> *node) {


        Node<TYPE> **array = (Node<TYPE> **) malloc(sizeof(Node<TYPE> *) * size);
        int *var = new int(0);
        Add_all(data, root, array, var);


        array[*var] = nullptr; //Flag to find and of arr

        return array;
    }

    void Add_all(TYPE *data, Node<TYPE> *node, Node<TYPE> **array, int *var) {
        current = node;
        if (node != nullptr) {
            if (compare(*node->Get_data(), *data) == 0) { // ==
                array[*var] = node;
                (*var)++;
            }
        } else {
            return;
        }
        if (node->Get_left() != nullptr) {
            Add_all(data, node->Get_left(), array, var);
        }
        if (node->Get_right() != nullptr) {
            Add_all(data, node->Get_right(), array, var);
        }

    }
    //End


    //Search for element with data = data
    Node<TYPE> *Find_element(TYPE *data, Node<TYPE> *node) {
        current = node;
        if (node != nullptr) {
            if (compare(*node->Get_data(), *data) == 0) {
                return node;
            }
        } else {
            return nullptr;
        }
        if (node->Get_left() != nullptr) {
            if (Find_element(data, node->Get_left()) != nullptr) {
                return Find_element(data, node->Get_left());
            }
        }
        if (node->Get_right() != nullptr) {
            if (Find_element(data, node->Get_right()) != nullptr) {
                return Find_element(data, node->Get_right());
            }
        }

        return nullptr;
    }
    //End

    //Search for a subtree
    bool Find_subtree(Binary_tree<TYPE> subtree) {

        Node<TYPE> **array = Find_all_elements(subtree.root->Get_data(), root);
        Binary_tree<TYPE> new_tree;


        int i = 0;
        while (array[i] != nullptr) {
            new_tree = Get_subtree(array[i]);
            if (new_tree == subtree) {
                return true;
            }
            i++;
        }

        return false;
    }
    //End

    //Comparison trees
    friend bool operator==(Binary_tree<TYPE> &first, Binary_tree<TYPE> &second) {
        return first.Check_all(first.Get_root(), second.Get_root());
    }

    bool Check_all(Node<TYPE> *first, Node<TYPE> *second) {//Recursively check all nodes
        if ((first != nullptr && second != nullptr) && compare(*first->Get_data(), *second->Get_data()) == 0) {
            if (first->Get_left() != nullptr && second->Get_left() != nullptr) {
                if (!Check_all(first->Get_left(), second->Get_left())) {
                    return false;
                }
            }
            if (first->Get_right() != nullptr && second->Get_right() != nullptr) {
                if (!Check_all(first->Get_right(), second->Get_right())) {
                    return false;
                }
            }
            return true;
        } else {
            return false;
        }

    }

    friend bool operator!=(const Binary_tree<TYPE> &first, const Binary_tree<TYPE> &second) {
        return !(first == second);
    }
    //End

    //Get NLR as string
    string Get_NLR() {

        stringstream tmp;

        return Get_NLR_string_stream(root, &tmp)->str();

    }

    stringstream *Get_NLR_string_stream(Node<TYPE> *node, stringstream *stream) {
        current = node;
        if (node != nullptr) {
            if (node != root) {
                (*stream) << "(\n" << *node->Get_data() << ") " << endl;
            } else {
                (*stream) << "{\n" << *node->Get_data() << "} " << endl;
            }
        }

        if (node->Get_left() != nullptr) {
            Get_NLR_string_stream(node->Get_left(), stream);
        }
        if (node->Get_right() != nullptr) {
            Get_NLR_string_stream(node->Get_right(), stream);
        }

        return stream;
    }
    //End

    //Get LRN as string
    string Get_LRN() {

        stringstream tmp;

        return Get_LRN_string_stream(root, &tmp)->str();

    }

    stringstream *Get_LRN_string_stream(Node<TYPE> *node, stringstream *stream) {
        current = node;
        if (node->Get_left() != nullptr) {
            Get_LRN_string_stream(node->Get_left(), stream);
        }
        if (node->Get_right() != nullptr) {
            Get_LRN_string_stream(node->Get_right(), stream);
        }
        if (node != nullptr) {
            if (node != root) {
                (*stream) << "(\n" << *node->Get_data() << ") " << endl;
            } else {
                (*stream) << "{\n" << *node->Get_data() << "} " << endl;
            }
        }

        return stream;
    }
    //End

    //Input string contains < if we want go to the left child or > for the right child
    Node<TYPE> *Get_node_by_string(string path) {
        if (size <= 0) {
            throw 42;//Empty tree
        }
        current = root;
        const char *array = path.c_str();
        for (int i = 0; i < path.size(); i++) {
            if (array[i] == '>') {
                if (current->Get_right() != nullptr) {
                    current = current->Get_right();
                } else {
                    throw 108; //Invalid or incorrect string was passed
                }
            } else if (array[i] == '<') {
                if (current->Get_left() != nullptr) {
                    current = current->Get_left();
                } else {
                    throw 108; //Invalid or incorrect string was passed
                }
            }
        }

        return current;
    }

    //Relative searcch
    Node<TYPE> *Get_node_by_string_relative(string path, Node<TYPE> *node) {
        if (size <= 0) {
            throw 42;//Empty tree
        }
        current = node;
        const char *array = path.c_str();
        for (int i = 0; i < path.size(); i++) {
            if (array[i] == '>') {
                if (current->Get_right() != nullptr) {
                    current = current->Get_right();
                } else {
                    throw 108; //Invalid or incorrect string was passed
                }
            } else if (array[i] == '<') {
                if (current->Get_left() != nullptr) {
                    current = current->Get_left();
                } else {
                    throw 108; //Invalid or incorrect string was passed
                }
            }
        }

        return current;
    }
    //end

    //Generating from string ( TYPE PARAM PARAM PARAM ) ( TYPE PARAM PARAM PARAM )
    void Generate_tree_from_string(string str) {
        const char *array = str.c_str();
        string word;
        int count = 0; // Count of words for different types

        bool is_params;

        string params[10];

        int i = 0;
        int j = 1;

        while (i < str.size()) {
            if (array[i] != ' ' && array[i] != '\n') {
                word += array[i];
            } else { //Find word
                if (word == "(") {

                    is_params = true;

                    j = 1;
                    word = "";
                    while (word != ")") {
                        if (array[i + j] != ' ') {
                            word += array[i + j];
                        } else {
                            params[count] = word;
                            word = "";
                            count++;
                        }
                        j++;
                    }

                    if (params[0] == "Person") {
                        this->Add_element(new Person(params[1], params[2], params[3], atoi(params[4].c_str())));
                    } else if (params[0] == "Teacher") {
                        this->Add_element(
                                new Teacher(params[1], params[2], params[3], atoi(params[4].c_str()), params[5]));
                    } else if (params[0] == "Student") {
                        this->Add_element(
                                new Student(params[1], params[2], params[3], atoi(params[4].c_str()), params[5]));
                    } else {
                        throw 108; //Invalid or incorrect string was passed
                    }
                    count = 0;
                }
                word = "";
            }
            i += j;
            j = 1;
        }

        if (!is_params) {
            throw 108;
        }

    }
};

#endif //BINARY_TREE_BINARY_TREE_H
