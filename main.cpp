#include <iostream>
#include "Person.h"
#include "Binary_tree.h"
#include <string>
#include "Tests.h"


using namespace std;

int inc(int a) {
    return (a + 1);
}


int number_of_tree = 1;
int selected_tree = 1;

Binary_tree<Person> **arr = (Binary_tree<Person> **) malloc(number_of_tree * sizeof(Binary_tree<Person> *));

const int NUMBER_OF_ANSWERS = 10;
const string welcome_msg = "1)Create tree\n"
                           "2)Select tree\n"
                           "3)Add element to selected tree\n"
                           "4)Remove element from selected tree\n"
                           "5)Output tree NLR\n"
                           "6)Output tree LRN\n"
                           "7)Create tree by string\n"
                           "8)Use merge\n"
                           "9)Get subtree\n"
                           "10)Get node by path\n"
                           "0)Quit\n"
                           ">";

void work_with_people(int num) {


    string first_name, middle_name, last_name, group, department;
    int age, passport_series, passport_number;

    cout << "Enter first name>";
    cin >> first_name;
    cout << endl;
    cout << "Enter middle name>";
    cin >> middle_name;
    cout << endl;
    cout << "Enter last name>";
    cin >> last_name;
    cout << endl;

    do {
        cout << "Enter age>";
        if (!(cin >> age) || (cin.peek() != '\n') || ((age < 0) || (age > 150))) {
            cin.clear();
            while (cin.get() != '\n');
            cout << "Incorrect input!" << endl;
            age = -100;
        }

    } while ((age < 0) || (age > 150));
    cout << endl;

    do {
        cout << "Enter passport [series] [number] if you want>";
        if (!(cin >> passport_series >> passport_number) || (cin.peek() != '\n') ||
            ((passport_series < 0) || (passport_number < 0))) {
            cin.clear();
            while (cin.get() != '\n');
            cout << "Incorrect input!" << endl;
            passport_number = -100;
            passport_series = -100;
        }

    } while ((passport_series < 0) || (passport_number < 0));
    cout << endl;

    if (num == 1) { //Student
        cout << "Enter group>";
        cin >> group;
        cout << endl;

        arr[selected_tree]->Add_element(
                new Student(first_name, middle_name, last_name, age, group, passport_series, passport_number));
    } else if (num == 2) {//Teacher
        cout << "Enter Department>";
        cin >> department;
        cout << endl;

        arr[selected_tree]->Add_element(
                new Teacher(first_name, middle_name, last_name, age, department, passport_series, passport_number));
    } else if (num == -1) {//Deleete Student
        cout << "Enter group>";
        cin >> group;
        cout << endl;

        if (arr[selected_tree]->Delete_element(new Student(first_name, middle_name, last_name, age, group, 0, 0))) {
            cout << "Element was deleted!" << endl;
        } else {
            cout << "Element was not found!" << endl;
        }
    } else if (num == -2) {
        cout << "Enter Department>";
        cin >> department;
        cout << endl;

        if (arr[selected_tree]->Delete_element(new Teacher(first_name, middle_name, last_name, age, department, passport_series, passport_number))) {
            cout << "Element was deleted!" << endl;
        } else {
            cout << "Element was not found!" << endl;
        }
    }
}

int main() {

    cout << "Do you want to run tests? y/n" << endl;
    char c;
    cin >> c;
    if ((c == 'y') || (c == 'Y')) {
        Tests t;
        t.Start_tests();
    }

    arr[0] = new Binary_tree<Person>();
    selected_tree = 0;

    int ans = 0;

    do {
        do {
            cout << welcome_msg;
            if (!(cin >> ans) || (cin.peek() != '\n') || ((ans < 0) || (ans > NUMBER_OF_ANSWERS))) {
                cin.clear();
                while (cin.get() != '\n');
                cout << "Incorrect input!" << endl;
                ans = -100;
            }
        } while ((ans < 0) || (ans > NUMBER_OF_ANSWERS));

        switch (ans) {

            case 1: {
                number_of_tree++;
                arr = (Binary_tree<Person> **) realloc(arr, number_of_tree * sizeof(Binary_tree<Person> *));
                arr[number_of_tree - 1] = new Binary_tree<Person>();
                break;
            }

            case 2: {
                int num = -100;

                do {
                    cout << "You have " << number_of_tree << " trees\nEnter number of tree\n>";
                    if (!(cin >> num) || (cin.peek() != '\n') || ((num < 0) || (num > number_of_tree))) {
                        cin.clear();
                        while (cin.get() != '\n');
                        cout << "Incorrect input!" << endl;
                        num = -100;
                    }
                } while ((num < 0) || (num > number_of_tree));
                selected_tree = num - 1;
                break;
            }
            case 3: {
                int num = -100;

                do {
                    cout << "What you want to enter?\n1 - Student, 2 - Teacher\n>";
                    if (!(cin >> num) || (cin.peek() != '\n') || ((num < 0) || (num > 2))) {
                        cin.clear();
                        while (cin.get() != '\n');
                        cout << "Incorrect input!" << endl;
                        num = -100;
                    }
                } while ((num < 0) || (num > 2));

                work_with_people(num);
                break;
            }

            case 4: {
                int num = -100;

                do {
                    cout
                            << "What you want to delete? <<Now it search person by age ONLY>> \n1 - Student, 2 - Teacher\n>";
                    if (!(cin >> num) || (cin.peek() != '\n') || ((num < 0) || (num > 2))) {
                        cin.clear();
                        while (cin.get() != '\n');
                        cout << "Incorrect input!" << endl;
                        num = -100;
                    }
                } while ((num < 0) || (num > 2));

                try {
                    work_with_people(-1 * num);
                } catch (int error_oode) {
                    if (error_oode == 42) {
                        cout << "Empty tree was passed!" << endl;
                    } else {
                        cout << "Unknown error!" << endl;
                        return 0;
                    }
                }
                break;
            }

            case 5: {
                try {
                    arr[selected_tree]->NLR(arr[selected_tree]->Get_root());
                } catch (int error_oode) {
                    if (error_oode == 42) {
                        cout << "Empty tree was passed!" << endl;
                    } else {
                        cout << "Unknown error!" << endl;
                        return 0;
                    }
                }
                break;
            }

            case 6: {
                try {
                    arr[selected_tree]->LRN(arr[selected_tree]->Get_root());
                } catch (int error_oode) {
                    if (error_oode == 42) {
                        cout << "Empty tree was passed!" << endl;
                    } else {
                        cout << "Unknown error!" << endl;
                        return 0;
                    }
                }
                break;
            }

            case 7: {
                cout << "Enter string format ( [TYPE] [DATA0] [DATA1] ) ( [TYPE] [DATA0] [DATA1] ) \n"
                     << "MAX 1000 symbols First person is ROOT \n>";

                cin.clear();
                cin.get(); // '\n' ???????

                char tree_string[1000];
                cin.getline(tree_string, 1000);

                string str = tree_string;

                Binary_tree<Person> *tmp_tree = new Binary_tree<Person>;

                try {
                    tmp_tree->Generate_tree_from_string(str);
                    if (!tmp_tree->Is_empty()) {
                        number_of_tree++;
                        arr = (Binary_tree<Person> **) realloc(arr, number_of_tree * sizeof(Binary_tree<Person> *));
                        arr[number_of_tree - 1] = tmp_tree;
                    }
                } catch (int error_code) {
                    if (error_code == 108) {
                        cout << "Incorrect string format!" << endl;
                    } else {
                        cout << "Unknown error!" << endl;
                        return 0;
                    }
                }

                break;
            }

            case 8: {
                int first, second;
                do {
                    cout << "You have " << number_of_tree
                         << " trees\nWhich trees you want to merge?"
                         << "[first tree] [second tree] >";
                    if (!(cin >> first) || !(cin >> second) || (cin.peek() != '\n') ||
                        ((first < 0) || (first > number_of_tree)) || ((second < 0) || (second > number_of_tree))) {
                        cin.clear();
                        while (cin.get() != '\n');
                        cout << "Incorrect input!" << endl;
                        first = -100;
                        second = -100;
                    }
                } while (((first < 0) || (first > number_of_tree)) || ((second < 0) || (second > number_of_tree)));

                try {
                    Binary_tree<Person> *new_tree = new Binary_tree<Person>;
                    *new_tree = arr[first - 1]->Merge(*arr[second - 1]);
                    if (!new_tree->Is_empty()) {
                        number_of_tree++;
                        arr = (Binary_tree<Person> **) realloc(arr, number_of_tree * sizeof(Binary_tree<Person> *));
                        arr[number_of_tree - 1] = new_tree;
                    }
                } catch (int error_code) {
                    if (error_code == 42) {
                        cout << "One of trees is empty!" << endl;
                    } else {
                        cout << "Unknown error!" << endl;
                        return 0;
                    }
                }

                break;
            }

            case 9: {
                string path;

                cout << "Enter path to the node\n"
                     << "< - left\n"
                     << "> - right\n"
                     << "Enter '.' if you want to get root\n"
                     << ">";
                cin >> path;

                try {
                    arr[selected_tree]->Get_subtree(arr[selected_tree]->Get_node_by_string(path));
                } catch (int error_code) {
                    if (error_code == 108) {
                        cout << "Incorrect string format!" << endl;
                    } else if (error_code == 42) {
                        cout << "Empty tree was passed!" << endl;
                    } else {
                        cout << "Unknown error!" << endl;
                        return 0;
                    }
                }
            }

            case 10: {
                string path;

                cout << "Enter path to the node\n"
                     << "< - left\n"
                     << "> - right\n"
                     << "Enter '.' if you want to get root\n"
                     << ">";
                cin >> path;

                try {
                    cout << *arr[selected_tree]->Get_node_by_string(path)->Get_data() << endl;
                } catch (int error_code) {
                    if (error_code == 108) {
                        cout << "Incorrect string format!" << endl;
                    } else if (error_code == 42) {
                        cout << "Empty tree was passed!" << endl;
                    } else {
                        cout << "Unknown error!" << endl;
                        return 0;
                    }
                }
            }

        }


    } while (ans != 0);

    return 0;
}