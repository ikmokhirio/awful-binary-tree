//
// Created by ikmokhirio on 16.05.2019.
//

#include "Tests.h"

Tests::Tests() {

}

Person func(Person p) {
    p.Set_age(15);
    return p;
}

bool big(Person a) {
    return (a.Get_age() > 5);
}

bool Tests::Map_test() {
    cout << "Map test" << endl;

    Binary_tree<Person> test_tree;
    test_tree.Add_element(new Student("aaa", "bbb", "ccc", 25, "G", 0, 0));
    test_tree.Add_element(new Student("aaa2", "bbb2", "ccc2", 5, "G2", 0, 0));
    test_tree.Add_element(new Student("aaa3", "bbb3", "ccc3", 35, "G3", 0, 0));

    cout << "Test tree:" << endl;
    try {

        test_tree.NLR(test_tree.Get_root());
    } catch (int error_code) {
        if (error_code == 42) {
            cout << "Empty tree!" << endl;
        } else {
            cout << "Unknown error!" << endl;
        }
        cout << "Map test was failed!" << endl;
        return false;
    }

    cout << endl << endl;

    Binary_tree<Person> expected_tree;
    expected_tree.Add_element(new Student("aaa", "bbb", "ccc", 15, "G", 0, 0));
    expected_tree.Add_element(new Student("aaa2", "bbb2", "ccc2", 15, "G2", 0, 0));
    expected_tree.Add_element(new Student("aaa3", "bbb3", "ccc3", 15, "G3", 0, 0));

    cout << "Expected tree:" << endl;
    try {
        expected_tree.NLR(expected_tree.Get_root());
    } catch (int error_code) {
        if (error_code == 42) {
            cout << "Empty tree!" << endl;
        } else {
            cout << "Unknown error!" << endl;
        }
        cout << "Map test was failed!" << endl;
        return false;
    }

    cout << "Result tree:" << endl;

    try {
        test_tree.Map(test_tree.Get_root(), func);
        test_tree.NLR(test_tree.Get_root());
        if (test_tree == expected_tree) {
            cout << "Map test was passed!" << endl;
            return true;
        } else {
            cout << "Map test was failed!" << endl;
            return false;
        }
    } catch (int error_code) {
        if (error_code == 42) {
            cout << "Empty tree!" << endl;
        } else {
            cout << "Unknown error!" << endl;
        }
        cout << "Map test was failed!" << endl;
        return false;
    }


}

bool Tests::Where_test() {
    cout << "Where test" << endl;

    Binary_tree<Person> test_tree;
    test_tree.Add_element(new Student("aaa", "bbb", "ccc", 25, "G", 0, 0));
    test_tree.Add_element(new Student("aaa2", "bbb2", "ccc2", 5, "G2", 0, 0));
    test_tree.Add_element(new Student("aaa3", "bbb3", "ccc3", 35, "G3", 0, 0));

    cout << "Test tree:" << endl;
    try {

        test_tree.NLR(test_tree.Get_root());
    } catch (int error_code) {
        if (error_code == 42) {
            cout << "Empty tree!" << endl;
        } else {
            cout << "Unknown error!" << endl;
        }
        cout << "Where test was failed!" << endl;
        return false;
    }

    cout << endl << endl;

    Binary_tree<Person> expected_tree;
    expected_tree.Add_element(new Student("aaa", "bbb", "ccc", 25, "G", 0, 0));
    expected_tree.Add_element(new Student("aaa3", "bbb3", "ccc3", 35, "G3", 0, 0));

    cout << "Expected tree:" << endl;
    try {
        expected_tree.NLR(expected_tree.Get_root());
    } catch (int error_code) {
        if (error_code == 42) {
            cout << "Empty tree!" << endl;
        } else {
            cout << "Unknown error!" << endl;
        }
        cout << "Where test was failed!" << endl;
        return false;
    }

    cout << "Result tree:" << endl;

    try {
        Binary_tree<Person> result = test_tree.Where(big);
        result.NLR(result.Get_root());
        if (result == expected_tree) {
            cout << "Where test was passed!" << endl;
            return true;
        } else {
            cout << "Where test was failed!" << endl;
            return false;
        }
    } catch (int error_code) {
        if (error_code == 42) {
            cout << "Empty tree!" << endl;
        } else {
            cout << "Unknown error!" << endl;
        }
        cout << "Where test was failed!" << endl;
        return false;
    }
}

bool Tests::Merge_test() {
    cout << "Merge test" << endl;

    Binary_tree<Person> first_tree;
    first_tree.Add_element(new Student("aaa", "bbb", "ccc", 25, "G", 0, 0));

    cout << "First test tree:" << endl;
    try {

        first_tree.NLR(first_tree.Get_root());
    } catch (int error_code) {
        if (error_code == 42) {
            cout << "Empty tree!" << endl;
        } else {
            cout << "Unknown error!" << endl;
        }
        cout << "Merge test was failed!" << endl;
        return false;
    }

    Binary_tree<Person> second_tree;
    second_tree.Add_element(new Student("aaa", "bbb", "ccc", 25, "G", 0, 0));
    second_tree.Add_element(new Student("aaa2", "bbb2", "ccc2", 5, "G2", 0, 0));
    second_tree.Add_element(new Student("aaa3", "bbb3", "ccc3", 35, "G3", 0, 0));

    cout << "Second test tree:" << endl;
    try {

        second_tree.NLR(second_tree.Get_root());
    } catch (int error_code) {
        if (error_code == 42) {
            cout << "Empty tree!" << endl;
        } else {
            cout << "Unknown error!" << endl;
        }
        cout << "Merge test was failed!" << endl;
        return false;
    }

    cout << endl << endl;

    Binary_tree<Person> expected_tree;
    expected_tree.Add_element(new Student("aaa", "bbb", "ccc", 25, "G", 0, 0));
    expected_tree.Add_element(new Student("aaa", "bbb", "ccc", 25, "G", 0, 0));
    expected_tree.Add_element(new Student("aaa2", "bbb2", "ccc2", 5, "G2", 0, 0));
    expected_tree.Add_element(new Student("aaa3", "bbb3", "ccc3", 35, "G3", 0, 0));

    cout << "Expected tree:" << endl;
    try {
        expected_tree.NLR(expected_tree.Get_root());
    } catch (int error_code) {
        if (error_code == 42) {
            cout << "Empty tree!" << endl;
        } else {
            cout << "Unknown error!" << endl;
        }
        cout << "Merge test was failed!" << endl;
        return false;
    }

    cout << "Result tree:" << endl;

    try {
        Binary_tree<Person> result = first_tree.Merge(second_tree);
        result.NLR(result.Get_root());
        if (result == expected_tree) {
            cout << "Merge test was passed!" << endl;
            return true;
        } else {
            cout << "Merge test was failed!" << endl;
            return false;
        }
    } catch (int error_code) {
        if (error_code == 42) {
            cout << "Empty tree!" << endl;
        } else {
            cout << "Unknown error!" << endl;
        }
        cout << "Merge test was failed!" << endl;
        return false;
    }
}

bool Tests::Subtree_test() {
    cout << "Subtree test" << endl;

    Binary_tree<Person> test_tree;
    test_tree.Add_element(new Student("aaa", "bbb", "ccc", 25, "G", 0, 0));
    test_tree.Add_element(new Student("aaa2", "bbb2", "ccc2", 5, "G2", 0, 0));
    test_tree.Add_element(new Student("aaa2", "bbb2", "ccc2", 10, "G2", 0, 0));
    test_tree.Add_element(new Student("aaa3", "bbb3", "ccc3", 35, "G3", 0, 0));

    cout << "Test tree:" << endl;
    try {

        test_tree.NLR(test_tree.Get_root());
    } catch (int error_code) {
        if (error_code == 42) {
            cout << "Empty tree!" << endl;
        } else {
            cout << "Unknown error!" << endl;
        }
        cout << "Subtree test was failed!" << endl;
        return false;
    }

    cout << endl << endl;

    Binary_tree<Person> expected_tree;
    expected_tree.Add_element(new Student("aaa2", "bbb2", "ccc2", 5, "G2", 0, 0));
    expected_tree.Add_element(new Student("aaa2", "bbb2", "ccc2", 10, "G2", 0, 0));

    cout << "Expected tree:" << endl;
    try {
        expected_tree.NLR(expected_tree.Get_root());
    } catch (int error_code) {
        if (error_code == 42) {
            cout << "Empty tree!" << endl;
        } else {
            cout << "Unknown error!" << endl;
        }
        cout << "Subtree test was failed!" << endl;
        return false;
    }

    cout << "Result tree:" << endl;

    try {
        Binary_tree<Person> result = test_tree.Get_subtree(new Student("aaa2", "bbb2", "ccc2", 5, "G2", 0, 0));
        result.NLR(result.Get_root());
        if (result == expected_tree) {
            cout << "Subtree test was passed!" << endl;
            return true;
        } else {
            cout << "Subtree test was failed!" << endl;
            return false;
        }
    } catch (int error_code) {
        if (error_code == 42) {
            cout << "Empty tree!" << endl;
        } else {
            cout << "Unknown error!" << endl;
        }
        cout << "Subtree test was failed!" << endl;
        return false;
    }
}

bool Tests::Subtree_search_test() {
    cout << "Subtree search test" << endl;

    Binary_tree<Person> test_tree;
    test_tree.Add_element(new Student("aaa", "bbb", "ccc", 25, "G", 0, 0));
    test_tree.Add_element(new Student("aaa2", "bbb2", "ccc2", 5, "G2", 0, 0));
    test_tree.Add_element(new Student("aaa2", "bbb2", "ccc2", 10, "G2", 0, 0));
    test_tree.Add_element(new Student("aaa3", "bbb3", "ccc3", 35, "G3", 0, 0));

    cout << "Test tree:" << endl;
    try {

        test_tree.NLR(test_tree.Get_root());
    } catch (int error_code) {
        if (error_code == 42) {
            cout << "Empty tree!" << endl;
        } else {
            cout << "Unknown error!" << endl;
        }
        cout << "Subtree search test was failed!" << endl;
        return false;
    }

    cout << endl << endl;

    Binary_tree<Person> subtree;
    subtree.Add_element(new Student("aaa2", "bbb2", "ccc2", 5, "G2", 0, 0));
    subtree.Add_element(new Student("aaa2", "bbb2", "ccc2", 10, "G2", 0, 0));

    cout << "Subtree:" << endl;
    try {
        subtree.NLR(subtree.Get_root());
    } catch (int error_code) {
        if (error_code == 42) {
            cout << "Empty tree!" << endl;
        } else {
            cout << "Unknown error!" << endl;
        }
        cout << "Subtree search test was failed!" << endl;
        return false;
    }

    cout << "Expected result: " << true << endl;

    cout << "Result: ";

    try {
        bool result = test_tree.Find_subtree(subtree);
        cout << result << endl;
        if (result == true) {
            cout << "Subtree search test was passed!" << endl;
            return true;
        } else {
            cout << "Subtree search test was failed!" << endl;
            return false;
        }
    } catch (int error_code) {
        if (error_code == 42) {
            cout << "Empty tree!" << endl;
        } else {
            cout << "Unknown error!" << endl;
        }
        cout << "Subtree search test was failed!" << endl;
        return false;
    }
}

bool Tests::Element_search_test() {
    cout << "Element search test" << endl;

    Binary_tree<Person> test_tree;
    test_tree.Add_element(new Student("aaa", "bbb", "ccc", 25, "G", 0, 0));
    test_tree.Add_element(new Student("aaa2", "bbb2", "ccc2", 5, "G2", 0, 0));
    test_tree.Add_element(new Student("aaa2", "bbb2", "ccc2", 10, "G2", 0, 0));
    test_tree.Add_element(new Student("aaa3", "bbb3", "ccc3", 35, "G3", 0, 0));

    cout << "Test tree:" << endl;
    try {

        test_tree.NLR(test_tree.Get_root());
    } catch (int error_code) {
        if (error_code == 42) {
            cout << "Empty tree!" << endl;
        } else {
            cout << "Unknown error!" << endl;
        }
        cout << "Element search test was failed!" << endl;
        return false;
    }

    cout << endl << endl;

    cout << "Element: " << endl;

    Student *search_st = new Student("aaa2", "bbb2", "ccc2", 10, "G2", 0, 0);

    cout << *search_st << endl;

    cout << "Expected result: pointer to element with value = ELEMENT" << endl;

    cout << "Result:" << endl;

    try {
        Node<Person> *result = test_tree.Find_element(search_st, test_tree.Get_root());
        cout << *result->Get_data() << endl;
        if (*result->Get_data() == *search_st) {
            cout << "Element search test was passed!" << endl;
            return true;
        } else {
            cout << "Element search test was failed!" << endl;
            return false;
        }
    } catch (int error_code) {
        if (error_code == 42) {
            cout << "Empty tree!" << endl;
        } else {
            cout << "Unknown error!" << endl;
        }
        cout << "Element search test was failed!" << endl;
        return false;
    }
}

bool Tests::String_save_test() {
    cout << "String save test" << endl;

    Binary_tree<Person> test_tree;
    test_tree.Add_element(new Student("aaa", "bbb", "ccc", 25, "G", 0, 0));
    test_tree.Add_element(new Student("aaa2", "bbb2", "ccc2", 5, "G2", 0, 0));
    test_tree.Add_element(new Student("aaa3", "bbb3", "ccc3", 35, "G3", 0, 0));

    cout << "Test tree:" << endl;
    try {

        test_tree.NLR(test_tree.Get_root());
    } catch (int error_code) {
        if (error_code == 42) {
            cout << "Empty tree!" << endl;
        } else {
            cout << "Unknown error!" << endl;
        }
        cout << "String save test was failed!" << endl;
        return false;
    }

    cout << endl << endl;

    cout << "Expected string:" << endl;

    string expected_string = "{\n"
                             "Student\n"
                             "First name: aaa\n"
                             "Middle name: bbb\n"
                             "Last name: ccc\n"
                             "Age: 25\n"
                             "Group: G\n"
                             "} \n"
                             "(\n"
                             "Student\n"
                             "First name: aaa2\n"
                             "Middle name: bbb2\n"
                             "Last name: ccc2\n"
                             "Age: 5\n"
                             "Group: G2\n"
                             ") \n"
                             "(\n"
                             "Student\n"
                             "First name: aaa3\n"
                             "Middle name: bbb3\n"
                             "Last name: ccc3\n"
                             "Age: 35\n"
                             "Group: G3\n"
                             ") \n";

    cout << expected_string << endl;

    cout << "Result string:" << endl;

    try {
        string result = test_tree.Get_NLR();
        cout << result << endl;
        if (result == expected_string) {
            cout << "String save test was passed!" << endl;
            return true;
        } else {
            cout << "String save test was failed!" << endl;
            return false;
        }
    } catch (int error_code) {
        if (error_code == 42) {
            cout << "Empty tree!" << endl;
        } else {
            cout << "Unknown error!" << endl;
        }
        cout << "String save test was failed!" << endl;
        return false;
    }
}

bool Tests::String_read_test() {
    cout << "String read test" << endl;

    cout << endl << endl;

    cout << "Input string:" << endl;

    string input = "( Student aaa bbb ccc 25 G 0 0 ) ( Student aaa2 bbb2 ccc2 5 G2 0 0 )";

    cout << input << endl;

    Binary_tree<Person> expected;
    expected.Add_element(new Student("aaa", "bbb", "ccc", 25, "G", 0, 0));
    expected.Add_element(new Student("aaa2", "bbb2", "ccc2", 5, "G2", 0, 0));

    cout << "Expected tree:" << endl;
    try {
        expected.NLR(expected.Get_root());
    } catch (int error_code) {
        if (error_code == 42) {
            cout << "Empty tree!" << endl;
        } else {
            cout << "Unknown error!" << endl;
        }
        cout << "String read test was failed!" << endl;
        return false;
    }

    cout << "Result tree:" << endl;

    try {
        Binary_tree<Person> result;
        result.Generate_tree_from_string(input);
        result.NLR(result.Get_root());
        if (result == expected) {
            cout << "String read test was passed!" << endl;
            return true;
        } else {
            cout << "String read test was failed!" << endl;
            return false;
        }
    } catch (int error_code) {
        if (error_code == 42) {
            cout << "Empty tree!" << endl;
        } else {
            cout << "Unknown error!" << endl;
        }
        cout << "String read test was failed!" << endl;
        return false;
    }
}

bool Tests::Select_node_test() {
    cout << "Select node test" << endl;

    Binary_tree<Person> test_tree;
    test_tree.Add_element(new Student("aaa", "bbb", "ccc", 25, "G", 0, 0));
    test_tree.Add_element(new Student("aaa2", "bbb2", "ccc2", 5, "G2", 0, 0));
    test_tree.Add_element(new Student("aaa2", "bbb2", "ccc2", 5, "G2", 0, 0));
    test_tree.Add_element(new Student("aaa2", "bbb2", "ccc2", 7, "G2", 0, 0));

    cout << "Test tree:" << endl;
    try {
        test_tree.NLR(test_tree.Get_root());
    } catch (int error_code) {
        if (error_code == 42) {
            cout << "Empty tree!" << endl;
        } else {
            cout << "Unknown error!" << endl;
        }
        cout << "Select node test was failed!" << endl;
        return false;
    }

    cout << endl << endl;

    cout << "Input string:" << endl;

    string input = "<>";

    cout << input << endl;

    cout << "Expected node: " << endl;

    Student *expected = new Student("aaa2", "bbb2", "ccc2", 7, "G2", 0, 0);


    cout << "Result node:" << endl;

    try {
        Node<Person> *result;
        result = test_tree.Get_node_by_string(input);
        cout << *result->Get_data() << endl;
        if (*result->Get_data() == *expected) {
            cout << "Select node test was passed!" << endl;
            return true;
        } else {
            cout << "Select node test was failed!" << endl;
            return false;
        }
    } catch (int error_code) {
        if (error_code == 42) {
            cout << "Empty tree!" << endl;
        } else {
            cout << "Unknown error!" << endl;
        }
        cout << "Select node test was failed!" << endl;
        return false;
    }
}

void Tests::Start_tests() {
    results[0] = Map_test();
    cout << endl << endl;
    results[1] = Where_test();
    cout << endl << endl;
    results[2] = Merge_test();
    cout << endl << endl;
    results[3] = Subtree_test();
    cout << endl << endl;
    results[4] = Subtree_search_test();
    cout << endl << endl;
    results[5] = Element_search_test();
    cout << endl << endl;
    results[6] = String_save_test();
    cout << endl << endl;
    results[7] = String_read_test();
    cout << endl << endl;
    results[8] = Select_node_test();


    int good = 0;
    int bad = 0;

    for (int i = 0; i < number_of_tests; i++) {
        if (results[i]) {
            good++;
        } else {
            bad++;
        }
    }

    if (good == number_of_tests) {
        cout << "All tests were passed" << endl;
    } else if (bad == number_of_tests) {
        cout << "All tests were failed" << endl;
    } else {
        cout << good << " tests were passed\n"
             << bad << " tests were failed\n";
    }

    cout << endl << endl << endl;
}


