//
// Created by ikmokhirio on 04.05.2019.
//

#include "Person.h"
#include <iostream>

using namespace std;


Person::Person(string first_name, string middle_name, string last_name, int age, int passport_series,
               int passport_number) {
    this->first_name = first_name;
    this->middle_name = middle_name;
    this->last_name = last_name;
    this->age = age;
    this->pass->Set_number(passport_number);
    this->pass->Set_series(passport_series);
}

Passport *Person::Get_passport() {
    return pass;
}

void Person::Set_age(int age) {
    this->age = age;
}

void Person::Set_first_name(string first_name) {
    this->first_name = first_name;
}

void Person::Set_middle_name(string middle_name) {
    this->middle_name = middle_name;
}

void Person::Set_last_name(string last_name) {
    this->last_name = last_name;
}

int Person::Get_age() {
    return age;
}

string Person::Get_middle_name() {
    return middle_name;
}

string Person::Get_first_name() {
    return first_name;
}

string Person::Get_last_name() {
    return last_name;
}

stringstream Person::Show() {
    stringstream out;
    out << "Person\nFirst name: " << this->Get_first_name()
        << "\nMiddle name: " << this->Get_middle_name()
        << "\nLast name: " << this->Get_last_name()
        << "\nAge: " << this->Get_age() << endl;
    if ((this->pass->Get_series() != 0) && (this->pass->Get_number() != 0)) {
        out << "Passport series: " << this->pass->Get_series()
            << "  Passport number: " << this->pass->Get_number()
            << endl;
    }
    return out;
}

//=======OUTPUT============
ostream &operator<<(ostream &out, Person &person) {
    out << person.Show().str();
    return out;
}


//=======COMPARSING========
bool operator>(Person first, Person second) {
    return (first.Get_age() > second.Get_age());
}

bool operator>=(Person first, Person second) {
    return (first.Get_age() >= second.Get_age());
}

bool operator<(Person first, Person second) {
    return (first.Get_age() < second.Get_age());
}

bool operator<=(Person first, Person second) {
    return (first.Get_age() <= second.Get_age());
}

bool operator==(Person first, Person second) {
    return (first.Get_age() == second.Get_age());
}

bool operator!=(Person first, Person second) {
    return (first.Get_age() != second.Get_age());
}