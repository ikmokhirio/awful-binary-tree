//
// Created by ikmokhirio on 04.05.2019.
//

#ifndef BINARY_TREE_PASSPORT_H
#define BINARY_TREE_PASSPORT_H


class Passport {
private:
    int series;
    int number;
public:
    Passport(int series, int number);

    int Get_series();

    int Get_number();

    void Set_series(int series);

    void Set_number(int number);
};

#endif //BINARY_TREE_PASSPORT_H
