//
// Created by ikmokhirio on 04.05.2019.
//

#include <iostream>
#include "Teacher.h"

using namespace std;

Teacher::Teacher(string first_name, string middle_name, string last_name, int age, string department,
                 int passport_series, int passport_number) : Person(first_name, middle_name, last_name, age,
                                                                    passport_series, passport_number) {
    this->department = department;
}

stringstream Teacher::Show() {
    stringstream out;
    out << "Teacher\nFirst name: " << this->Get_first_name()
        << "\nMiddle name: " << this->Get_middle_name()
        << "\nLast name: " << this->Get_last_name()
        << "\nAge: " << this->Get_age()
        << "\nDepartment: " << this->Get_department()
        << endl;

    if ((this->Get_passport()->Get_number() != 0) && (this->Get_passport()->Get_series() != 0)) {
        out << "Passport series: " << this->Get_passport()->Get_series()
            << "  Passport number: " << this->Get_passport()->Get_number() << endl;
    }
    return out;
}

ostream &operator<<(ostream &out, Teacher &teacher) {
    out << teacher.Show().str();
    return out;
}

void Teacher::Set_department(string department) {
    this->department = department;
}

string Teacher::Get_department() {
    return department;
}