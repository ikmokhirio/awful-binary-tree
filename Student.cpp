//
// Created by ikmokhirio on 04.05.2019.
//

#include <iostream>
#include "Student.h"

using namespace std;

Student::Student(string first_name, string middle_name, string last_name, int age, string group, int passport_series,
                 int passport_number) : Person(first_name, middle_name, last_name, age, passport_series,
                                               passport_number) {
    this->group = group;

}

ostream &operator<<(ostream &out, Student &student) {
    out << student.Show().str();
    return out;
}

stringstream Student::Show() {
    stringstream out;
    out << "Student\nFirst name: " << this->Get_first_name()
        << "\nMiddle name: " << this->Get_middle_name()
        << "\nLast name: " << this->Get_last_name()
        << "\nAge: " << this->Get_age()
        << "\nGroup: " << this->Get_group() << endl;
    if ((this->Get_passport()->Get_number() != 0) && (this->Get_passport()->Get_series() != 0)) {
        out << "Passport series: " << this->Get_passport()->Get_series()
            << "  Passport number: " << this->Get_passport()->Get_number() << endl;
    }
    return out;
}

void Student::Set_group(string group) {
    this->group = group;
}

string Student::Get_group() {
    return group;
}
