//
// Created by ikmokhirio on 04.05.2019.
//

#include "Person.h"
#include <sstream>

#ifndef BINARY_TREE_TEACHER_H
#define BINARY_TREE_TEACHER_H

class Teacher : public Person {
private:
    string department;
public:
    Teacher(string first_name, string middle_name, string last_name, int age, string department,
            int passport_series = 0, int passport_number = 0);

    friend std::ostream &operator<<(std::ostream &, Teacher &teacher);

    stringstream Show();

    string Get_department();

    void Set_department(string department);
};



#endif //BINARY_TREE_TEACHER_H
