#include "Binary_tree.h"
#include <iostream>
//
// Created by ikmokhirio on 16.05.2019.
//

using namespace std;

#ifndef BINARY_TREE_TESTS_H
#define BINARY_TREE_TESTS_H


class Tests {
private:
    const int static number_of_tests = 9;

    bool results[number_of_tests];
public:
    Tests();

    bool Map_test();

    bool Where_test();

    bool Merge_test();

    bool Subtree_test();

    bool Subtree_search_test();

    bool Element_search_test();

    bool String_save_test();

    bool String_read_test();

    bool Select_node_test();

    void Start_tests();
};


#endif //BINARY_TREE_TESTS_H
