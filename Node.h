//
// Created by ikmokhirio on 04.05.2019.
//

#ifndef BINARY_TREE_NODE_H
#define BINARY_TREE_NODE_H

template<class TYPE>
class Node {
private:
    Node *left;
    Node *right;
    Node *parent;
    TYPE *data;
public:
    Node(TYPE *data) {
        this->data = data;
        left = nullptr;
        right = nullptr;
        parent = nullptr;
    }

    Node() {
        parent = nullptr;
        left = nullptr;
        right = nullptr;
        data = nullptr;
    }

    ~Node() {
        if (data != nullptr) {
            if (parent != nullptr) {
                if (*data >= *(parent->Get_data())) {
                    parent->Set_right(nullptr);
                } else {
                    parent->Set_left(nullptr);
                }
            }
        }
    }

    void Set_left(Node<TYPE> *left) {
        this->left = left;
    }

    void Set_right(Node<TYPE> *right) {
        this->right = right;
    }

    void Set_parent(Node<TYPE> *parent) {
        this->parent = parent;
    }

    Node *Get_left() {
        return left;
    }

    Node *Get_right() {
        return right;
    }

    Node *Get_parent() {
        return parent;
    }

    TYPE *Get_data() {
        return (data);
    }

    void Set_data(TYPE *data) {
        this->data = data;
    }
};

#endif //BINARY_TREE_NODE_H